from dotenv import load_dotenv
import logging
import os
import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from app.api.routers.basicqa import basicqa_router
from app.api.routers.translation import translation_router
from instrument import instrument

load_dotenv()
do_not_instrument = os.getenv("INSTRUMENT_DSPY", "true") == "false"
if not do_not_instrument:
    instrument()

app = FastAPI(title="DSPy x FastAPI", description="System Version: 1.0.0")
app.include_router(basicqa_router, prefix="/api/basicqa", tags=["Basic_QA"])
app.include_router(translation_router, prefix="/api/translation", tags=["Translation"])

@app.get("/")
async def root():
    return {"system_version": "Your system version information here"}

environment = os.getenv("ENVIRONMENT", "dev")  # Default to 'development' if not set
if environment == "dev":
    logger = logging.getLogger("uvicorn")
    logger.warning("Running in development mode - allowing CORS for all origins")
    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

if __name__ == "__main__":
    uvicorn.run(app="main:app", host="0.0.0.0", reload=True)

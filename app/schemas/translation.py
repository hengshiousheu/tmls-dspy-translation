from pydantic import BaseModel
from pydantic import BaseModel, ValidationError, validator, Field
from enum import Enum

class Language(str, Enum):
    zh = 'zh'        # 繁体中文
    en = 'en'        # 英文
    ja = 'ja'        # 日文
    ko = 'ko'        # 韩文
    id = 'id'        # 印尼文
    vi = 'vi'        # 越南文
    th = 'th'        # 泰文

    @property
    def full_name(self):
        language_full_names = {
            'zh': 'Traditional Chinese',
            'en': 'English',
            'ja': 'Japanese',
            'ko': 'Korean',
            'id': 'Indonesian',
            'vi': 'Vietnamese',
            'th': 'Thai',
        }
        return language_full_names[self.value]
    # 提供枚举值的描述，虽然这不会直接显示在Swagger UI的枚举下拉列表中，但可以在API描述中提及

class TranslationRequest(BaseModel):
    query: str = Field(..., example="Hello world")
    source_language: Language = Field(..., description="Source language of the text to be translated.")
    target_language: Language = Field(..., description="Target language for the translation.")

    class Config:
        json_schema_extra = {
            "example": {
                "query": "Hello world",
                "source_language": "en",
                "target_language": "zh",
            }
        }

    @validator('source_language', 'target_language', pre=True)
    def must_be_supported_language(cls, value):
        if value not in Language.__members__.values():
            raise ValueError(f'{value} is not a supported language')
        return value
"""Endpoints."""
from fastapi import APIRouter
from app.utils.basicqa_modules import BasicQA

basicqa_router = APIRouter()

@basicqa_router.get("/healthcheck")
async def healthcheck():

    return {"message": "Thanks for playing."}

@basicqa_router.get("/zero-shot-query")
async def zero_shot_query(query: str):
    basicqa = BasicQA()
    pred = basicqa(query)

    return {
        "question": query,
        "predicted answer": pred.answer,
    }
"""Endpoints."""
from fastapi import APIRouter
from app.utils.translation_modules import TranslationModule
from app.schemas.translation import TranslationRequest

translation_router = APIRouter()

@translation_router.get("/healthcheck")
async def healthcheck():

    return {"message": "Translation Routring Alive."}

@translation_router.post("/zero-shot-query", description="Translate text from one language to another. Supported languages are: Traditional Chinese (zh), English (en), Japanese (ja), Korean (ko), Indonesian (id), Vietnamese (vi), Thai (th).")
async def zero_shot_query(request: TranslationRequest):

    translation = TranslationModule()

    # 在调用 translation 函数之前，获取语言的完整字句
    source_language_full = request.source_language.full_name
    target_language_full = request.target_language.full_name

    pred = translation(
        query=request.query, 
        source_language=source_language_full, 
        target_language=target_language_full
    )

    return {
        "source_sentence": request.query,
        "source_language": source_language_full,
        "target_language": target_language_full,
        "target_sentence": pred.translated_text,
    }
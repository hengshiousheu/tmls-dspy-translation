import os
import dspy
from dotenv import load_dotenv

# Load ENV config
load_dotenv()

# Global settings
ollama_base_url = os.getenv("OLLAMA_BASE_URL", "localhost")
ollama_model_name = os.getenv("OLLAMA_MODEL_NAME", "phi")
ollama_lm = dspy.OllamaLocal(model=ollama_model_name, base_url=ollama_base_url)

# DSPy Setting
dspy.settings.configure(lm=ollama_lm)

#
class GenerateAnswer(dspy.Signature):
    """Answer questions with short factoid answers."""

    question = dspy.InputField()
    answer = dspy.OutputField(desc="often between 1 and 5 words")

class BasicQA(dspy.Module):
    def __init__(self):
        super().__init__()
        self.generate_answer = dspy.ChainOfThought(GenerateAnswer)
        # self.prog = dspy.Predict(GenerateAnswer)

    def forward(self, question):
        """forward 方法呼叫 __call__ ，類似於 pytorch 中的工作方式。"""
        prediction = self.generate_answer(question=question)
        return dspy.Prediction(answer=prediction.answer)
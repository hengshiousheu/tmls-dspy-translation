import os
import dspy
from dotenv import load_dotenv

# Load ENV config
load_dotenv()

# Global settings
environment = os.getenv("ENVIRONMENT", "dev")

# Global settings
if environment == "dev":
    ollama_base_url = os.getenv("OLLAMA_BASE_URL", "localhost")
    ollama_model_name = os.getenv("OLLAMA_MODEL_NAME", "phi")
    ollama_lm = dspy.OllamaLocal(model=ollama_model_name, base_url=ollama_base_url)
    # DSPy Setting
    dspy.settings.configure(lm=ollama_lm)

else:
    vllm_base_url = os.getenv("VLLM_BASE_URL", "localhost")
    vllm_base_url_port = os.getenv("VLLM_BASE_URL_port", "8000")
    vllm_model_name = os.getenv("VLLM_MODEL_NAME", "Heng666/dolphin-2.6-mistral-7b-dpo-laser-1-epoch")
    vllm_lm = dspy.HFClientVLLM(model=vllm_model_name, port=vllm_base_url_port, url="http://"+vllm_base_url)
    # DSPy Setting
    dspy.settings.configure(lm=vllm_lm)

class Translation(dspy.Signature):
    """You are a highly skilled translator with expertise in many languages. Your task is to identify the source language of the text I provide and accurately translate it into the specified target language while preserving the meaning, tone, and nuance of the original text. Please maintain proper grammar, spelling, and punctuation in the translated version."""
    source_language = dspy.InputField(desc="The language of the source text.")
    source_text = dspy.InputField(desc="The text to be translated, provided in the source language.")
    target_language = dspy.InputField(desc="The language into which the text should be translated.")
    translated_text = dspy.OutputField(desc="The result of translating the source text into the target language.")

class TranslationModule(dspy.Module):
    def __init__(self):
        super().__init__()
        self.translated_context = dspy.Predict(Translation)

    def forward(self, query, source_language, target_language):
        # 翻譯
        translation = self.translated_context(
            source_text=query, 
            source_language=source_language, 
            target_language=target_language, 
            )

        return dspy.Prediction(translated_text=translation.translated_text)
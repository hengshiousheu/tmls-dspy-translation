import pytest
from fastapi.testclient import TestClient
from main import app  # 确保从正确的地方导入您的FastAPI app实例

client = TestClient(app)

# 定义测试数据
test_cases = [
    ("en", "Hello world", "English", "Traditional Chinese", "你好，世界"),
    ("ja", "こんにちは世界", "Japanese", "Traditional Chinese", "你好，世界"),
    ("ko", "안녕하세요 세계", "Korean", "Traditional Chinese", "你好，世界"),
    ("id", "Halo dunia", "Indonesian", "Traditional Chinese", "你好，世界"),
    ("vi", "Xin chào thế giới", "Vietnamese", "Traditional Chinese", "你好，世界"),
    ("th", "สวัสดีชาวโลก", "Thai", "Traditional Chinese", "你好，世界"),
]

@pytest.mark.parametrize("source_lang, query, source_full, target_full, expected_translation", test_cases)
def test_translation_to_chinese(source_lang, query, source_full, target_full, expected_translation):
    response = client.post("/api/translation/zero-shot-query", json={
        "query": query,
        "source_language": source_lang,
        "target_language": "zh_TW"
    })
    assert response.status_code == 200
    data = response.json()
    assert data["source_sentence"] == query
    assert data["source_language"] == source_full
    assert data["target_language"] == target_full

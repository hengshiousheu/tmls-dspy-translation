# FastAPI 包裝器為 DSPy

## 介紹

這個項目是一個為了與 StanfordNLP 開發的 [DSPy](https://github.com/stanfordnlp/dspy) 框架整合而設計的 [FastAPI](https://github.com/tiangolo/fastapi) 包裝器，提供了一個直接的例子來構建一個具有 DSPy 能力的 FastAPI 後端。獨特之處在於，這個實現完全是本地的，利用 [Ollama](https://github.com/ollama/ollama) 來處理語言模型，以及 [Arize Phoenix](https://github.com/Arize-ai/phoenix) 用於可觀察性層。這樣的設置確保了所有操作都在本機進行，無需外部雲服務，增強了隱私性和數據安全性。

## 功能

- **本地執行**：一切運行在您的本機上，確保數據隱私和安全。沒有外部雲服務參與。
- **Ollama 整合**：默認使用 Ollama 的 phi-2 語言模型。然而，現在支持可配置的 LLM 支持，允許用戶在 .env 文件或 Docker Compose 文件中指定所需的語言模型。
- **Arize Phoenix**：整合了 Arize Phoenix 進行可觀察性，提供實時監控和分析以追蹤和提升模型性能和系統健康。
- **零擊查詢**：允許用戶通過一個簡單的 GET 請求使用 DSPy 進行零擊查詢。
- **編譯查詢**：通過 GET 允許查詢的編譯以優化執行。
- **編譯程序**：通過 POST 請求提供了一個編譯 DSPy 程序的接口，促進了與語言模型的更複雜互動。

## 架構

FastAPI 包裝器將 DSPy 與 Ollama、Arize Phoenix 無縫整合，為需要先進自然語言處理能力的應用程序提供了堅固的後端。以下是我們本地設置中組件如何互動：

- **DSPy 框架**：處理語言模型提示和權重的優化，提供了一個用於與語言模型編程的精緻接口。
- **Ollama**：作為語言模型和嵌入模型的後端，使得自然語言理解和生成強大且高效。
- **Arize Phoenix**：Phoenix 通過可視化每次

對編譯過的 DSPy 模塊的調用的底層結構，使您的 DSPy 應用程序可觀察。

這種本地設置不僅增強了數據安全和隱私，而且為開發人員提供了一個靈活而強大的環境，以構建先進的 NLP 應用程序。

## 安裝

### 先決條件

- Docker 和 Docker-Compose
- Git（可選，用於克隆倉庫）
- Ollama，遵循 [readme](https://github.com/ollama/ollama) 來設置和運行本地 Ollama 實例。

### 克隆倉庫

首先，將倉庫克隆到您的本地機器（如果已有項目文件，則跳過此步驟）。

```bash
git clone https://github.com/Heng666/dspy-fastapi.git
cd dspy-fastapi
```

### 開始本地開發

首先，設置環境：

```bash
poetry config virtualenvs.in-project true
poetry install
poetry shell
```

在目錄中的 .env 文件中指定您的環境變量。
示例 .env 文件：
```yml
ENVIRONMENT=<your_environment_value>
INSTRUMENT_DSPY=<true or false>
COLLECTOR_ENDPOINT=<your_arize_phoenix_endpoint>
OLLAMA_BASE_URL=<your_ollama_instance_endpoint>
OLLAMA_MODEL_NAME=<your_llm_model_name>
```

第三，執行測試案例程式碼
```bash
pytest
```

最後，運行此命令啟動 FastAPI 服務器：
```bash
python main.py
```

### 本地開發（Docker 封裝）
```bash
DOCKER_BUILDKIT=1 docker buildx build --platform linux/amd64,linux/arm64 -t xiuxiumycena/tmls-dspy-translation:0.0.2 . --push
```

開發階段可以使用，
backend 會直接 build . 來處理
```bash
docker-compose -f compose-dev.yml up 
```

生產階段可以使用
backend 會採用 docker hug 上面 image 來處理
```bash
docker-compose -f compose-prod.yml up 
```

### 使用 Docker-Compose 開始

該項目現在支持 Docker Compose 以便於設置和部署，包括後端服務和 Arize Phoenix 用於查詢跟踪。

1. 在 .env 文件中配置您的環境變量或直接修改 compose 文件。
2. 確保 Docker 已安裝且運行。
3. 運行命令 `docker-compose -f compose.yml up` 來啟動後端和 Phoenix 的服務。
4. 可以使用 [OpenAPI-Swagger](http://0.0.0.0:8000/docs) 查看後端文檔。
5. 可以使用 [Phoenix UI](http://0.0.0.0:6006) 查看跟踪。
7. 完成後，運行 `docker compose down` 以關閉服務。

## 使用

啟動 FastAPI 服務器後，您可以如下與 API 端點互動：

| 方法   | 端點                  | 描述                                  | 示例                                                                                      |
|--------|-----------------------|---------------------------------------|------------------------------------------------------------------------------------------|
| GET    | `/healthcheck`        | 检查翻译服务是否活跃。                | `curl http://<your_address>:8000/api/translation/healthcheck`                           |
| POST   | `/zero-shot-query`    | 从一种语言翻译文本到另一种语言。      | `curl -X POST http://<your_address>:8000/api/translation/zero-shot-query -H "Content-Type: application/json" -d '{"query": "Hello world", "source_language": "en", "target_language": "zh_TW"}'` |


確保將 `<your-query>` 和 `<your-program>` 替換為您希望執行的實際查詢和 DSPy程序。